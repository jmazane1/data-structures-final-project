#include <iostream>
#include "Incident.h"
#include "Team.h"
#include <map>
#include <string>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <algorithm>
#include <cstring>

using namespace std;

string to_lower(string);
vector<string> split(string, char);
void usage(string, int);
void print(string, int, string, string, map<string, Team>);

string PROGRAM_NAME; // program name global variable

// Main Execution
int main(int argc, char * argv[]){
    PROGRAM_NAME = argv[0];
    map<string, Team> teams; // Holds NFL teams
    string line;
    for (int i = 2000; i < 2017; i++) {
        // opens team stats file for year
        string file_name = "./team_season_stats/" + to_string(i) + ".csv";
        ifstream stat_file;
        stat_file.open (file_name);
        getline(stat_file, line);
        // adds all the given stats for each year to the given team
        while (getline(stat_file, line)) {
            vector<string> r = split(line, ',');
            if (i == 2008 || i == 2012 || i == 2013 || i == 2014 || i == 2016) {
                teams[to_lower(r[1])].setWP(stof(r[5], 0), i-2000);
                teams[to_lower(r[1])].setPF(stoi(r[6], 0), i-2000);
                teams[to_lower(r[1])].setPA(stoi(r[7], 0), i-2000);
                teams[to_lower(r[1])].setOSRS(stof(r[12], 0), i-2000);
                teams[to_lower(r[1])].setDSRS(stof(r[13], 0), i-2000);
            } else {
                teams[to_lower(r[1])].setWP(stof(r[4], 0), i-2000);
                teams[to_lower(r[1])].setPF(stoi(r[5], 0), i-2000);
                teams[to_lower(r[1])].setPA(stoi(r[6], 0), i-2000);
                teams[to_lower(r[1])].setOSRS(stof(r[11], 0), i-2000);
                teams[to_lower(r[1])].setDSRS(stof(r[12], 0), i-2000);
            }
        }
        // opens penalties file
        stat_file.close();
        file_name = "./team_season_stats/penalties" + to_string(i) + ".csv";
        stat_file.open (file_name);
        getline(stat_file, line);
        // adds the number of penalties per season for each team
        while (getline(stat_file, line)) {
            vector<string> r = split(line, ',');
            vector<string> z = split(r[1], ' ');
            teams[to_lower(z[z.size() - 1])].setPEN(stoi(r[23], 0), i-2000);
        }
        stat_file.close();
    }
    // opens arrest incidents file
    ifstream incidents_file;
    incidents_file.open("./ArrestIncidents.csv");
    getline(incidents_file, line);
    // parses incidents file and adds all incidents to the list in the right team
    while (getline(incidents_file, line)) {
        vector<string> r = split(line, ',');
        for (int i = 0; i < 32; i++) {
            if (to_lower(r[1]) == TeamNames[i][1]) {
                vector<string> z = split(r[0], '/');
                if ((stoi(z[2]) - 2000) == 17)
                    continue;
                struct Incident incident = {TeamNames[i][0], r[2], to_lower(r[3]), r[4], r[5], stoi(z[2]) - 2000, r[6]};
                teams[TeamNames[i][0]].addIncident(incident);
            }
        }
    }
    // adds abbreviations and cities to map as another search method
    for (int i = 0; i < 32; ++i) {
        teams[TeamNames[i][1]] = teams[TeamNames[i][0]];
        teams[TeamNames[i][2]] = teams[TeamNames[i][0]];
    }
    // PARSING COMMAND LINE ARGUMENTS
    int argind = 1;
    string team_name = "";
    string stat = "";
    int year = -1;
    string position = "";
    // parses through flags
    while(argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-'){
        string arg = argv[argind++];
        switch(arg[1]){
            case 't':
                team_name = to_lower(argv[argind++]);
                break;
            case 's':
                stat = to_lower(argv[argind++]);
                break;
            case 'y':
                year = stoi(argv[argind++]);
                break;
            case 'p':
                position = to_lower(argv[argind++]);
                break;
            case 'h':
                usage(PROGRAM_NAME, 0);
            default:
                usage(PROGRAM_NAME, 1);
        }
    }
    // print out according stat line
    print(team_name, year, stat, position, teams);

}

// print function
void print(string team, int year, string stat, string position, map<string, Team> teams) {
    if (team == "") {
        if (position == "") {
            // loop through and print all stats
            for (int i = 0; i < 32; i++) {
                teams[TeamNames[i][0]].print();
            }
        } else {
            int n = 0;
            for (int i = 0; i < 32; i++) {
                n += teams[TeamNames[i][0]].getPosIncidents(position);
            }
            cout << n << endl;
        }
    } else {
        if (year == -1) {
            // print stats for all year
            if (stat == "") {
                // teams[team].printSeason(year - 2000);
            } else {
                // print specific stat line
                if(stat == "wp") {  // win percentage
                    teams[team].printWinPctAverage();
                } else if(stat == "osrs") { // offensive efficiency
                    teams[team].printOffEffAverage();
                } else if(stat == "dsrs") { // defensive efficiency
                    teams[team].printDefEffAverage();
                } else if(stat == "pen") {  // penalties
                    teams[team].printPenaltiesTotal();
                } else if(stat == "arr") {  // arrests
                    teams[team].printTotalArrests();
                } else {
                    usage(PROGRAM_NAME, 1);
                }
            }
        } else {
            if (stat == "") {
                teams[team].printSeason(year - 2000);
            } else {
                // print specific stat line
                if(stat == "wp") {  // win percentage
                    teams[team].printWinPctSeason(year-2000);
                } else if(stat == "osrs") { // offensive efficiency
                    teams[team].printOffEffSeason(year-2000);
                } else if(stat == "dsrs") { // defensive efficiency
                    teams[team].printDefEffSeason(year-2000);
                } else if(stat == "pen") {  // penalties
                    teams[team].printPenaltiesSeason(year-2000);
                } else if(stat == "arr") {  // arrests
                    teams[team].printSeasonArrests(year-2000);
                } else {
                    usage(PROGRAM_NAME, 1);
                }
            }
        }
    }
}

// Displays usage information
void usage(string prog, int status) {
    cerr << "Usage: " << prog << " [htsyp]" << endl;
    cerr << "Options:" << endl;
    cerr << "   -h          Display help message" << endl;
    cerr << "   -t team     Sort by a specific team" << endl;
    cerr << "   -s stat     Sort by a specific stat" << endl;
    cerr << "   -y year     Sort by a specific year" << endl;
    cerr << "   -p position Sort by a specific position" << endl;
    exit(status);
}

// Transforms a string to lowercase
string to_lower(string s) {
    transform(s.begin(), s.end(), s.begin(), ::tolower);
    return s;
}

// returns a vector of each section delimited by d, similar to cut
vector<string> split(string x, char d) {
    vector<string> v;
    string tmp = "";
    for(int i = 0; i < x.size(); ++i) {
        if (x[i] == d) {
            v.push_back(tmp);
            tmp = "";
        } else if(i + 1 == x.size()) {
            tmp = tmp + x[i];
            v.push_back(tmp);
        } else
            tmp = tmp + x[i];
    }
    return v;
}
