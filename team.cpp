/*
 * Team.cpp
 *  Implementation for Team class.
 *  Author: Louis Thiery and Jack Mazanec
 *
 *  cse-20312
 *  Final Project
 *  05/01/2017
 */


#include "Team.h" // Header file

// Constructor
Team::Team() {}

// Access Methods
list<Incident> Team::getIncidents(int season) { return incidents[season]; }
int Team::getPF(int season) { return PF[season]; }
int Team::getPA(int season) { return PA[season]; }
int Team::getPEN(int season) { return PEN[season]; }
float Team::getOSRS(int season) { return OSRS[season]; }
float Team::getDSRS(int season) { return DSRS[season]; }
float Team::getWP(int season) { return WP[season]; }

// Utility Methods
void Team::addIncident(Incident incident) { incidents[incident.season].push_back(incident); }
void Team::setPF(int value, int season) { PF[season] = value; }
void Team::setPA(int value, int season) { PA[season] = value; }
void Team::setPEN(int value, int season) { PEN[season] = value; }
void Team::setWP(float value, int season) { WP[season] = value; }
void Team::setOSRS(float value, int season) { OSRS[season] = value; }
void Team::setDSRS(float value, int season) { DSRS[season] = value; }

// Returns total incidents for this team over every year
int Team::getPosIncidents(string position) {
    int n = 0;
    // Loops through every year and adds incidents that meet position criteria
    for (int i = 0; i < 17; i++) {
        for (list<Incident>::const_iterator iterator = incidents[i].begin(), end = incidents[i].end(); iterator != end; ++iterator) {
            if (iterator->position == position) {
                n++;
            }
        }
    }
    return n;
}

// Print Methods

// Print stats for a given season
void Team::printSeasonStats(int season) {
    cout << (season + 2000) << " Season Stats" << std::endl;
    cout << "-----------------------------" << std::endl;
    cout << "Win Percentage:      " << WP[season] << endl;
    cout << "Points For:          " << PF[season] << endl;
    cout << "Points Against:      " << PA[season] << endl;
    cout << "Penalties Committed: " << PEN[season] << endl;
    cout << "Offensive Rating:    " << OSRS[season] << endl;
    cout << "Defensive Rating:    " << DSRS[season] << endl;
}

// Print incidents for a given season
void Team::printSeasonIncidents(int season) {
    cout << to_string(season + 2000) << " Season Incidents" << endl;
    cout << "-----------------------------" << endl;
    for (list<Incident>::const_iterator iterator = incidents[season].begin(), end = incidents[season].end(); iterator != end; ++iterator) {
        cout << iterator->player << " / " << iterator->position << " / " << iterator->type << " / " << iterator->category << endl;
        cout << "    " << iterator->description << endl;
    }
}

// Print both stats and incidents for a given season
void Team::printSeason(int season) {
    printSeasonStats(season);
    cout << endl;
    printSeasonIncidents(season);
}

// Print stats and incidents for all seasons
void Team::print() {
    for (int i = 0; i < 17; ++i) {
        printSeason(i);
        cout << endl;
    }
}

// Print the number of arrests for a given season
void Team::printSeasonArrests(int yr){
    cout << incidents[yr].size() << endl;
}

// Print total arrests
void Team::printTotalArrests(){
    int arrest_total = 0;
    for (int i = 0; i < 17; i++) 
        arrest_total += incidents[i].size();     

    cout << arrest_total << endl;
}

// Print offensive efficiency for a given season
void Team::printOffEffSeason(int yr) {
    cout << OSRS[yr] << endl;  
}

// Print defenseive efficiency for a given season
void Team::printDefEffSeason(int yr) {
    cout << DSRS[yr] << endl;  
}

// Print average offensive efficiency across all seasons
void Team::printOffEffAverage() {
    float OSRS_average = 0;
    for (int i = 0; i < 17; i++) { 
        OSRS_average += OSRS[i];
    }
    OSRS_average /= 17;
    cout << OSRS_average << endl;
}

// Print average defensive efficiency across all seasons
void Team::printDefEffAverage() {
    float DSRS_average = 0;
    for (int i = 0; i < 17; i++) { 
        DSRS_average += DSRS[i];
    }
    DSRS_average /= 17;
    cout << DSRS_average << endl;    
}

// Print win percentage for a given season
void Team::printWinPctSeason(int yr){
    cout << WP[yr] << endl;
}
void Team::printWinPctAverage(){
    float WP_average = 0;
    for (int i = 0; i < 17; i++) { 
        WP_average += WP[i];
    }
    WP_average /= 17;
    cout << WP_average << endl;     
}

// Print penalties for a given season
void Team::printPenaltiesSeason(int yr){
    cout << PEN[yr] << endl;
}

// Print total penalties across all seasons
void Team::printPenaltiesTotal() {
    int total_pen = 0;
    for (int i = 0; i < 17; i++) { 
        total_pen += PEN[i];
    }
    cout << total_pen << endl;   
}
