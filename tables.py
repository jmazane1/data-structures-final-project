#!/usr/bin/env python2.7

import os
import sys
import subprocess

# Question 1: Are teams with more arrests more successful?
# Make graph with line of best fit and plots of number of arrests against win pct for 17 years
# for each team
# ./NFL_ARRESTS -t team -s wp
# ./NFL_ARRESTS -t team -s arr

# Question 2: Are teams with more arrests more prone to commit penalties?
# Make graph with line of best fit and plots of number of arrests against number of penalties for 17 years
# ./NFL_ARRESTS -t team -s pen
# ./NFL_ARRESTS -t team -s arr

# Question 3: Is there a correlation between Defense and arrests?
# Make graph with line of best fit and plots of number of arrests against average DSRS for 17 years
# ./NFL_ARRESTS -t team -s dsrs
# ./NFL_ARRESTS -t team -s arr


# Question 4: Is there a correlation between Offense and arrests?
# Make graph with line of best fit and plots of number of arrests against average OSRS for 17 years
# ./NFL_ARRESTS -t team -s osrs
# ./NFL_ARRESTS -t team -s arr


# Question 5: What position is arrested the most?
# Make Pie char with position break up
# ./NFL_ARRESTS -p position

