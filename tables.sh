#!/bin/sh

# list of all nfl teams
teams=("cardinals" "falcons" "ravens" "bills" "bengals" "browns" "cowboys" "broncos" "lions" "packers" "colts" "chiefs" "texans" "jaguars" "rams" "chargers" "dolphins" "vikings" "patriots" "saints" "giants" "jets" "raiders" "eagles" "steelers" "49ers" "seahawks" "buccaneers" "titans" "redskins" "panthers" "bears")

# make and change the different sheets then call gnu plot at the end to generate the graphs
# $ cat > scatter.plt <<EOF
# set terminal png

# plot 'data.txt' \
# title "Values"
# EOF
# gnuplot scatter.plt > scatter.png 

# Question 1: Are teams with more arrests more successful?
# Make graph with line of best fit and plots of number of arrests against win pct for 17 years
# for each team

echo \# Win Percentage arrests > data1.csv
for i in "${teams[@]}"; do
	x=$(./NFL_ARRESTS -t $i -s wp)
	y=$(./NFL_ARRESTS -t $i -s arr)
	echo "$x,$y" >> data1.csv

done

# Question 2: Are teams with more arrests more prone to commit penalties?
# Make graph with line of best fit and plots of number of arrests against number of penalties for 17 years

echo \# penalties arrests > data2.csv
for i in "${teams[@]}"; do
	x=$(./NFL_ARRESTS -t $i -s pen)
	y=$(./NFL_ARRESTS -t $i -s arr)
	echo "$x,$y" >> data2.csv
done
cat > scatter2.plt <<EOF
set terminal png

plot 'data2.csv' \
title "Penalties Number vs. Arrest Number"
EOF
gnuplot scatter2.plt > scatter2.png 

# Question 3: Is there a correlation between Defense and arrests?
# Make graph with line of best fit and plots of number of arrests against average DSRS for 17 years

echo \# DSRS arrests > data3.csv
for i in "${teams[@]}"; do
	x=$(./NFL_ARRESTS -t $i -s dsrs)
	y=$(./NFL_ARRESTS -t $i -s arr)
	echo "$x,$y" >> data3.csv
done
cat > scatter3.plt <<EOF
set terminal png

plot 'data3.csv' \
title "DSRS vs. Arrest Number"
EOF
gnuplot scatter3.plt > scatter3.png 

# Question 4: Is there a correlation between Offense and arrests?
# Make graph with line of best fit and plots of number of arrests against average OSRS for 17 years
echo \# OSRS arrests > data4.csv
for i in "${teams[@]}"; do
	x=$(./NFL_ARRESTS -t $i -s osrs)
	y=$(./NFL_ARRESTS -t $i -s arr)
	echo "$x,$y" >> data4.csv
done
cat > scatter4.plt <<EOF
set terminal png

plot 'data4.csv' \
title "OSRS vs. Arrest Number"
EOF
gnuplot scatter4.plt > scatter4.png 

# Question 5: What position is arrested the most?
# Make Pie char with position break up
positions=("qb" "cb" "te" "dt" "s" "ot" "rb" "de" "wr" "og" "lb" "fb" "k" "p" "c")
echo \# Arrests by postion > data5.csv
for i in "${positions[@]}"; do
	x=$(./NFL_ARRESTS -p $i)
	echo "$i,$x" >> data5.csv
done

# Use GNU plot to make the new stuff




