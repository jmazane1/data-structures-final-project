// team.h - header for team class
// Author: Jack Mazanec
// Date: 4/17/17

// represents arrest record of teams

#ifndef TEAM_H
#define TEAM_H

#include "Incident.h"
#include <list>
#include <string>
#include <iostream>

using namespace std;

class Team {
    public :
        // Constructor
        Team();

        // Access Methods
        list<Incident> getIncidents(int);
        int getPF(int);
        int getPA(int);
        int getPEN(int);
        float getOSRS(int);
        float getDSRS(int);
        float getWP(int);

        // Utility Methods
        void addIncident(Incident);
        void setPF(int, int);
        void setPA(int, int);
        void setPEN(int, int);
        void setWP(float, int);
        void setOSRS(float, int);
        void setDSRS(float, int);
        int getPosIncidents(string);

        // Print Methods
        void printSeasonStats(int);
        void printSeasonIncidents(int);
        void printSeason(int);
        void print();
        void printTotalArrests();
        void printSeasonArrests(int);
        void printOffEffSeason(int);
        void printDefEffSeason(int);
        void printOffEffAverage();
        void printDefEffAverage();
        void printWinPctSeason(int);
        void printWinPctAverage();
        void printPenaltiesSeason(int);
        void printPenaltiesTotal();

    private :
        list<Incident> incidents[17];   // Incidents
        int PF[17];                     // Points For
        int PA[17];                     // Points Against
        int PEN[17];                    // Penalties Committed
        float WP[17];                   // Win Percentage
        float OSRS[17];                 // Offensive Rating
        float DSRS[17];                 // Defensive Rating
};

#endif
