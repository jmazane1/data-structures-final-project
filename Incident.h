/*
 * Incident.h
 *  Header file that contains Incident struct.
 *  Author: Louis Thiery and Jack Mazanec
 *
 *  cse-20312
 *  Final Project
 *  05/01/2017
 */

// Include Guard
#ifndef INCIDENT_H
#define INCIDENT_H

#include <string>

using namespace std;

struct Incident {
    string team;        // team offending player was playing for
    string player;      // name of the offending player
    string position;    // position offending player was playing
    string type;        // resolution of incident (arrested, charged, etc.)
    string category;    // type of crime committed (theft, murder, etc.)
    int season;         // year the incident took place
    string description; // description of incident
};

// contains name information (nickname, abbreviation, and city) for all 32 NFL teams
static const string TeamNames[32][3] = {
                                         {"cardinals", "ari", "arizona"},       {"falcons", "atl", "atlanta"},
                                         {"ravens", "bal", "Baltimore"},        {"bills", "buf", "buffalo"},
                                         {"bengals", "cin", "cincinnati"},      {"browns", "cle", "cleveland"},
                                         {"cowboys", "dal", "dallas"},          {"broncos", "den", "denver"},
                                         {"lions", "det", "detroit"},           {"packers", "gb", "green bay"},
                                         {"texans", "hou", "houston"},          {"colts", "ind", "indianapolis"},
                                         {"jaguars", "jac", "jacksonville"},    {"chiefs", "kc", "kansas city"},
                                         {"rams", "stl", "st. louis"},          {"chargers", "sd", "san diego"},
                                         {"dolphins", "mia", "miami"},          {"vikings", "min", "minnesota"},
                                         {"patriots", "ne", "new england"},     {"saints", "no", "new orleans"},
                                         {"giants", "nyg", "new york giants"},  {"jets", "nyj", "new york jets"},
                                         {"raiders", "oak", "oakland"},         {"eagles", "phi", "philadelphia"}, 
                                         {"steelers", "pit", "pittsburgh"},     {"49ers", "sf", "san francisco"},
                                         {"seahawks", "sea", "seattle"},        {"buccaneers", "tb", "tampa bay"}, 
                                         {"titans", "ten", "tennessee"},        {"redskins", "was", "washington"},
                                         {"bears", "chi", "chicago"},           {"panthers", "car", "carolina"}
                                       };

#endif
