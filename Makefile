## Makefile for Data Structures Arrests Project
## Author: Louis Thiery and Jack Mazanec
## Date: 05/01/2017

CC = g++
CFLAGS= -std=c++11 -Wall

NFL_ARRESTS: Team.o main.o
	$(CC) $(CFLAGS) $^ -o $@

Team.o: Team.cpp Team.h Incident.h
	@echo Compiling $@...
	@$(CC) $(CFLAGS) -c Team.cpp -o $@


main.o: main.cpp Team.h Incident.h
	@echo Compiling $@...
	@$(CC) $(CFLAGS) -c main.cpp -o $@

clean:
	rm *.o
	rm NFL_ARRESTS
